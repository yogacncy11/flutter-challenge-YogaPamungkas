import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:test_1st/ui/login/page_login.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    Timer(const Duration(seconds: 2), () {
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
            builder: (context) => const PageLogin(),
          ),
          (route) => false);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Image.asset(
            'assets/header-splash.png',
            height: 257.h,
            width: 375.w,
            fit: BoxFit.cover,
          ),
          Image.asset(
            'assets/logo.png',
            height: 96.h,
            width: 165.w,
          ),
          Image.asset(
            'assets/footer-splash.png',
            height: 267.h,
            width: 375.w,
            fit: BoxFit.cover,
          ),
        ],
      ),
    );
  }
}

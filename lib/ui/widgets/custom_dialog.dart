import 'package:flutter/material.dart';

import '../../shared/theme.dart';

Future customDialog({
  required BuildContext contex,
  required String tittle,
  required String content,
}) {
  return showDialog(
      context: contex,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(18),
          ),
          title: Text(
            tittle,
            style: blackTextStyle.copyWith(fontSize: 18),
          ),
          content: Text(
            content,
            style: greyTextStyle,
          ),
          actions: [
            TextButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text(
                  'Ok',
                  style: blackTextStyle,
                ))
          ],
        );
      });
}

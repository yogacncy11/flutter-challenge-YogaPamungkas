import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../shared/helper.dart';
import '../../shared/theme.dart';

class PasswordTextfield extends StatefulWidget {
  final TextEditingController controller;
  final String label;
  final String hintText;

  const PasswordTextfield({
    Key? key,
    required this.controller,
    required this.hintText,
    required this.label,
  }) : super(key: key);

  @override
  State<PasswordTextfield> createState() => _PasswordTextfieldState();
}

class _PasswordTextfieldState extends State<PasswordTextfield> {
  String passwordValue = '';
  bool isVisible = true;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          widget.label,
          style: blackTextStyle.copyWith(fontSize: 16),
        ),
        SizedBox(
          height: 6.h,
        ),
        TextFormField(
          cursorHeight: 20,
          controller: widget.controller,
          obscureText: isVisible,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          enableInteractiveSelection: true,
          onChanged: (String? value) {
            setState(() {
              passwordValue = value!;
            });
          },
          validator: (value) {
            if (!checkPassword(value!)) {
              return "Password must contain Uppercase, Lowercase,\nSpecial character, Number and min 6 character";
            } else {
              return null;
            }
          },
          decoration: InputDecoration(
              suffixIcon: IconButton(
                  onPressed: () {
                    setState(() {
                      isVisible = !isVisible;
                    });
                  },
                  icon: isVisible
                      ? const Icon(Icons.visibility)
                      : const Icon(Icons.visibility_off)),
              hintText: widget.hintText,
              hintStyle: greyTextStyle,
              contentPadding: const EdgeInsets.symmetric(
                vertical: 14,
              )),
        ),
      ],
    );
  }
}

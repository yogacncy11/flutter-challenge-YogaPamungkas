import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../shared/theme.dart';

class CustomButton extends StatelessWidget {
  final Function() onTap;
  final String label;
  final double width;
  const CustomButton(
      {Key? key,
      required this.onTap,
      required this.label,
      this.width = double.infinity})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        height: 50.h,
        width: width,
        decoration: BoxDecoration(
          color: const Color(0xff5C40CC),
          borderRadius: BorderRadius.circular(22),
        ),
        child: Center(
          child: Text(
            label,
            style: whiteTextStyle.copyWith(fontSize: 18.sm, fontWeight: medium),
          ),
        ),
      ),
    );
  }
}

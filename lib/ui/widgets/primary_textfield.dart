import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../shared/theme.dart';

class PrimaryTextField extends StatelessWidget {
  final TextEditingController controller;
  final String label;
  final String hintText;
  final String? Function(String?) onChange;
  final String? Function(String?) validate;
  const PrimaryTextField(
      {Key? key,
      required this.controller,
      required this.label,
      required this.hintText,
      required this.onChange,
      required this.validate})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          label,
          style: blackTextStyle.copyWith(fontSize: 16),
        ),
        SizedBox(
          height: 6.h,
        ),
        TextFormField(
          controller: controller,
          onChanged: onChange,
          validator: validate,
          cursorHeight: 20,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          enableInteractiveSelection: true,
          decoration: InputDecoration(
              hintText: hintText,
              hintStyle: greyTextStyle,
              contentPadding: const EdgeInsets.symmetric(
                vertical: 14,
              )),
        ),
      ],
    );
  }
}

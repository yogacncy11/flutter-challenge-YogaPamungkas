import 'package:flutter/material.dart';
import 'package:test_1st/ui/login/component/footer_login.dart';
import 'package:test_1st/ui/login/component/header_login.dart';
import 'package:test_1st/ui/login/component/input_section_login.dart';

class PageLogin extends StatelessWidget {
  const PageLogin({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          physics: const NeverScrollableScrollPhysics(),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: const [
              HeaderLogin(),
              InputSectionLogin(),
              FooterLogin(),
            ],
          ),
        ),
      ),
    );
  }
}

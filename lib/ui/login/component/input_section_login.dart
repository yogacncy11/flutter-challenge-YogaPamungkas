import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:test_1st/shared/helper.dart';
import 'package:test_1st/ui/widgets/custom_button.dart';
import 'package:test_1st/ui/widgets/custom_dialog.dart';
import 'package:test_1st/ui/widgets/password_textfield.dart';
import 'package:test_1st/ui/widgets/primary_textfield.dart';

import '../../../shared/theme.dart';

class InputSectionLogin extends StatefulWidget {
  const InputSectionLogin({Key? key}) : super(key: key);

  @override
  State<InputSectionLogin> createState() => _InputSectionLoginState();
}

class _InputSectionLoginState extends State<InputSectionLogin> {
  TextEditingController cUser = TextEditingController();
  TextEditingController cPassword = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(
        horizontal: 24,
        vertical: 50,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Sign In',
            style: blackTextStyle.copyWith(
              fontSize: 24.sm,
              fontWeight: medium,
            ),
          ),
          Text(
            'Please sign in to continue',
            style: greyTextStyle.copyWith(fontSize: 14.sm),
          ),
          SizedBox(
            height: 30.h,
          ),
          PrimaryTextField(
            onChange: (String? value) {
              setState(() {
                value!;
              });
              return null;
            },
            validate: (value) {
              if (value!.length <= 5) {
                return 'User ID must have min 6 character';
              }
              return null;
            },
            controller: cUser,
            label: 'Used ID',
            hintText: 'yoga',
          ),
          SizedBox(height: 20.h),
          PasswordTextfield(
            controller: cPassword,
            hintText: '12345',
            label: 'Password',
          ),
          SizedBox(
            height: 30.h,
          ),
          Align(
            alignment: Alignment.centerRight,
            child: CustomButton(
              onTap: () {
                if (cUser.text == '' && cPassword.text == '') {
                  customDialog(
                      contex: context,
                      tittle: 'Login Gagal',
                      content: 'User ID dan atau Password anda belum diisi.');
                } else if (checkPassword(cPassword.text) == false &&
                    cUser.text.length <= 5) {
                  customDialog(
                      contex: context,
                      tittle: 'Login Gagal',
                      content: 'Check Kembali User Id dan Password anda');
                } else {
                  customDialog(
                      contex: context,
                      tittle: 'Login Berhasil',
                      content: 'Selamat anda berhasil login');
                }
              },
              label: 'Login',
              width: 150.h,
            ),
          )
        ],
      ),
    );
  }
}

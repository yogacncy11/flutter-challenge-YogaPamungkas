import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class HeaderLogin extends StatelessWidget {
  const HeaderLogin({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      // mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Image.asset(
          'assets/header-login.png',
          width: 150.h,
          height: 150.h,
          fit: BoxFit.cover,
        ),
        SizedBox(
          width: 10.w,
        ),
        Image.asset(
          'assets/logo.png',
          width: 120.h,
          height: 120.h,
        ),
      ],
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../shared/theme.dart';

class FooterLogin extends StatelessWidget {
  const FooterLogin({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          "Don't have an account? ",
          style: greyTextStyle.copyWith(fontSize: 16.sm, fontWeight: light),
        ),
        Text(
          'Sign Up',
          style: redTextStyle.copyWith(fontSize: 16.sm, fontWeight: light),
        ),
      ],
    );
  }
}

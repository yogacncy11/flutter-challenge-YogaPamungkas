import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Color blackColor = const Color(0xff222222);
Color greyColor = const Color(0xff8E909C);
Color whiteColor = const Color(0xffffffff);
Color redColor = const Color(0xffEB5757);

TextStyle blackTextStyle = GoogleFonts.poppins(
  color: blackColor,
);

TextStyle greyTextStyle = GoogleFonts.poppins(
  color: greyColor,
);
TextStyle whiteTextStyle = GoogleFonts.poppins(
  color: whiteColor,
);
TextStyle redTextStyle = GoogleFonts.poppins(
  color: redColor,
);

FontWeight light = FontWeight.w300;
FontWeight regular = FontWeight.w400;
FontWeight medium = FontWeight.w500;
FontWeight semiBold = FontWeight.w600;
FontWeight bold = FontWeight.w700;
FontWeight extraBold = FontWeight.w800;
FontWeight black = FontWeight.w900;

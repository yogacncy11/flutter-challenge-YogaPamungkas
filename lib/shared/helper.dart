bool checkPassword(String pass) {
  final regexPassword =
      RegExp(r'(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*_]){6,}');
  if (pass.isEmpty) {
    return false;
  }
  int lenght = pass.length;
  bool hasPassword = pass.contains(regexPassword) && lenght >= 6;
  return hasPassword;
}
